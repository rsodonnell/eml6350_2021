function dxdt = dynamicsExam1Problem1(t, x, params)
% Dynamics of system in exam 1, problem 1

% Set parameters
a = params.a;
b = params.b;
c = params.c;

% System of dynamics
dxdt(1) = -a * x(1) + x(2);
dxdt(2) = -b * x(2) + x(3);
dxdt(3) = -c * x(3);
% dynamics = @(t,x) [-a * x(1) + x(2); -b * x(2) + x(3); -c * x(3)];

% State variable
% x(1) = x_1;
% x(2) = x_2;
% x(3) = u;