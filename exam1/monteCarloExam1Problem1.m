% monteCarloExam1Problem1.m
% Generates a monte carlo simulation of system dynamics with random parameters and initial conditions

% Housekeeping
close all
clc
clear all

% Create figure
parent = figure('Name', 'Problem 1');
tL = tiledlayout(parent, 3, 1, 'Padding', 'compact', 'TileSpacing', 'none');
title(tL, "Problem 1");

% Time span
tSpan = [0 100]; % time vector

% Run monte carlo 100 times
for ind = 1:100
    
    % Generate random vecotr of parameters and initial conditions
    randomVector = rand(6,1)*10;

    % Parameters
    % a = 1; b = 1; c = 1; % Generic parameters
    a = randomVector(1);
    b = randomVector(2);
    c = randomVector(3);

    % Initial condition
    % initCond = [0; 0; 0]; % Null initial condition (equilibrium -> check that state stays (0, 0, 0))
    initCond = [randomVector(4); randomVector(5); randomVector(6)];

    % Define dynamics
    % dxdt(1) = -a * x(1) + x(2);
    % dxdt(2) = -b * x(2) + x(3);
    % dxdt(3) = -c * x(3);
    dynamics = @(t,x) [-a * x(1) + x(2); -b * x(2) + x(3); -c * x(3)];

    % Simulate dynamics
    [time, state] = ode45(dynamics, tSpan, initCond);

    % Plot tiled layout
    % x_1(t)
    ax(1) = nexttile(1);
    plot(time,state(:,1))
    hold on
    title('x_1(t)')
    xlabel('t')
    ylabel('x_1')
    grid on

    % x_2(t)
    ax(2) = nexttile(2);
    plot(time,state(:,2))
    hold on
    title('x_2(t)')
    xlabel('t')
    ylabel('x_2')
    grid on

    % u(t)
    ax(3) = nexttile(3);
    plot(time,state(:,3))
    hold on
    title('u(t)')
    xlabel('t')
    ylabel('u')
    grid on

    linkaxes(ax, 'x')

end