% exam2Dynamics function that returns the dynamics of the collar and pendulum problem in exam 2
% FIRST ATTEMPT WITH UNITS AND t_s TO PREFORM INTEGRATION...
%         cX     |-> x, uX
% |------------====-------------|
% |------------====-------------|
%                \    uPhi
%                 \ /
%                | \    
%                |  \ l
%                |-- \    cPhi
%                |phi \  / 
%                |    ()m

%% Initialize variables
% Time
t_s(1) = 0; % (s) Time of dynamics

% Collar states (x)
x_m(1)             = 0; % Collar position
xDot_mps(1)        = 0; % Collar velocity
xDotDot_mps2(1)    = 0; % Collar acceleration
xDotDotDot_mps3(1) = 0; % Collar Jerk

% Pendulum states (phi)
phi_rad(1)             = 0; % Pendulum angle
phiDot_radps(1)        = 0; % Pendulum angular rate
phiDotDot_radps2(1)    = 0; % Pendulum angular acceleration
phiDotDotDot_radps3(1) = 0; % Pendulum anglular jerk

% Parameters (constant)
g_mps2       = 9.81; % (m/s^2) Acceleration due to gravity
% Collar
mX_kg        = 1; % (kg) Mass of collar, 1kg nominal
cX_kgps      = 1; % (kg/s) Coefficent of friction for collar, 1kg/s nominal
aX_nd        = 1; % (nd) coefficent of input force on collar
% Pendulum
mPhi_kg      = 1; % (kg) Mass of pendulum weight
cPhi_kgps    = 1; % (kg/s) Coefficent of friction for pendulum, 1kg/s nominal
l_m          = 1; % (m) Length of pendulum rod
aPhi_nd      = 1; % (nd) coefficent of input torque on pendulum

% Outputs
uX_N(1)         = 0; % (N) Output force on collar
uXDot_Nps(1)    = 0; % (N/s) Rate of change of output force on collar
uPhi_Nm(1)      = 0; % (Nm) Output torque on pendulum
uPhiDot_Nmps(1) = 0; % (Nm/s) Rate of change of output torque on pendulum

% Inputs (cotntrollable)
muX_N(1)    = 0; % (N) Input force to collar system
muPhi_Nm(1) = 0; % (Nm) Input torque to pendulum system

% Parameter vector (theta) note: unknown
theta(1)       = 0;
thetaHat(1)    = 0;
thetaHatDot(1) = 0;

% Desired trajectories
% Collar Constants
xDUpperBar_m = 1; % (m) Maximum aplitude of desired trajectory
xDLowerBar_m = xDUpperBar_m(1); % (m) Minimum amplitude of desired trajectory
fXD_Hz       = 1; % (Hz) Desired collar trajectory frequency
% Collar desired trajectory
xD_m(1)       = xDUpperBar_m * sin(2 * pi * fXD_Hz * t_s(1)); % (m) Desired position trajectory of collar
xDDot_m(1)    = 2 * pi * fXD_Hz * xDUpperBar_m * cos(2 * pi * fXD_Hz * t_s(1)); % (m) Desired velocity trajectory of collar
xDDotDot_m(1) = - (2 * pi * fXD_Hz)^2 * xDUpperBar_m * sin(2 * pi * fXD_Hz * t_s(1)); % (m) Desired velocity trajectory of collar
% Pendulum desired constants
phiDUpperBar_m = 0.79; % (rad) Maximum aplitude of desired trajectory (45deg nominal)
phiDLowerBar_m = xDUpperBar_m(1); % (rad) Minimum amplitude of desired trajectory
fPhiD_Hz       = 1; % (Hz) Desired pendulum trajectory frequency
% Pendulum desired trajectory
phiD_m(1)       = phiDUpperBar_m * sin(2 * pi * fPhiD_Hz * t_s(1)); % (m) Desired position trajectory of pendulum
phiDDot_m(1)    = 2 * pi * fPhiD_Hz * phiDUpperBar_m * cos(2 * pi * fPhiD_Hz * t_s(1)); % (m) Desired velocity trajectory of pendulum
phiDDotDot_m(1) = - (2 * pi * fPhiD_Hz)^2 * phiDUpperBar_m * sin(2 * pi * fPhiD_Hz * t_s(1)); % (m) Desired velocity trajectory of pendulum

%% Dynamics of collar and pendulum
xDotDot_mps2(t_s)     = (1 / (mX_kg + mPhi_kg * sin(phi_rad(t_s))^2)) * (-cX_kgps * xDot_mps(t_s) + mPhi_kg * g_mps2 * cos(phi_rad(t_s)) * sin(phi_rad(t_s)) + mPhi_kg * l_m * phiDot_radps(t_s))^2 + uX_N;
uXDot_Nps(t_s)        = - aX_nd * uX_N(t_s) + muX_N(t_s);
phiDotDot_radps2(t_s) = (1 / (mPhi_kg * l_m^2)) * (-cPhi_kgps * phiDot_radps(t_s) - mPhi_kg * l_m * g_mps2 * sind(phi_rad(t_s)) - mPhi_kg * l_m * cos(phi_rad(t_s)) * xDotDot_mps2(t_s) + uPhi_Nm(t_s));
uPhiDot_Nmps(t-s)     = - aPhi_nd * uPhi_Nm(t_s) + muPhi_Nm(t_s);

% Define errors
% Collar
eX_m(t_s)          = xD_m(t_s) - x_m(t_s);
eXDot_mps(t_s)     = xDDot_mps(t_s) - xDot_mps(t_s);
eXDotDot_mps2(t_s) = xDDotDot_mps2(t_s) - xDotDot_mps2(t_s);
% Pendulum
ePhi_rad(t_s)          = phi_rad(t_s) - x_m(t_s);
ePhiDot_radps(t_s)     = phiDDot_mps(t_s) - phiDot_radps(t_s);
ePhiDotDot_radps2(t_s) = phiDDotDot_radps2(t_s) - phiDotDot_radps2(t_s);

% Filter tracking error
% collar
alphaX_Hz = 1; % Filter error coefficent
rX_mps(t_s) = eXDot_mps(t_s) - alphaX_Hz * eX_m(t_s);
rXDot_mps2(t_s) = eXDotDot_mps2(t_s) + alphaX_Hz * eXDot(t_s);
% Pendelum
alphaPhi_Hz = 1; % Filter error coefficent
rPhi_radps(t_s) = ephiDot_radps(t_s) - alphaPhi_Hz * ePhi_m(t_s);
rPhiDot_radps2(t_s) = ePhiDotDot_radps2(t_s) + alphaPhi_Hz * ePhiDot_radps(t_s);

% Estimation parameters (parameter errors)
thetaTilde(t_s) = theta - thetaHat;
thetaTildeDot(t_s) = -thetaHatDot;

aTilde = a - aHat;
aTildeDot = - aHatDot;

uXTilde_N = uXD_N - uX_N;
uPhiTilde_Nm = uPhiD_Nm - uPhi_Nm;

%% Simulate dynamics

% Needs to be in a form like this...
dynamics = @(t,x) [-a * x(1) + x(2); ...
                   -b * x(2) + x(3); ...
                   -c * x(3)];

% Simulate dynamics
[time, state] = ode45(dynamics, tSpan, initCond);