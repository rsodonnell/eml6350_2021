% exam2DynamicsSim function that returns the dynamics of the collar and pendulum problem in exam 2
%
%         cX     |-> x, uX
% |------------====-------------|
% |------------====-------------|
%                \    uPhi
%                 \ /
%                | \    
%                |  \ l
%                |-- \    cPhi
%                |phi \  / 
%                |    ()m

%% Initialize variables
% Time
t_s(1) = 0; % (s) Time of dynamics

% % Collar states (x)
% x          = 0; % Collar position
% xDot       = 0; % Collar velocity
% xDotDot    = 0; % Collar acceleration
% xDotDotDot = 0; % Collar Jerk
% 
% % Pendulum states (phi)
% phi          = 0; % Pendulum angle
% phiDot       = 0; % Pendulum angular rate
% phiDotDot    = 0; % Pendulum angular acceleration
% phiDotDotDot = 0; % Pendulum anglular jerk
% 
% % Parameters (constant)
% g = 9.81; % (m/s^2) Acceleration due to gravity
% % Collar
% mX = 1; % (kg) Mass of collar, 1kg nominal
% cX = 1; % (kg/s) Coefficent of friction for collar, 1kg/s nominal
% aX = 1; % (nd) coefficent of input force on collar
% % Pendulum
% mPhi = 1; % (kg) Mass of pendulum weight
% cPhi = 1; % (kg/s) Coefficent of friction for pendulum, 1kg/s nominal
% l    = 1; % (m) Length of pendulum rod
% aPhi = 1; % (nd) coefficent of input torque on pendulum
% 
% % Outputs
% uX      = 0; % (N) Output force on collar
% uXDot   = 0; % (N/s) Rate of change of output force on collar
% uPhi    = 0; % (Nm) Output torque on pendulum
% uPhiDot = 0; % (Nm/s) Rate of change of output torque on pendulum
% 
% % Inputs (cotntrollable)
% muX   = 0; % (N) Input force to collar system
% muPhi = 0; % (Nm) Input torque to pendulum system
% 
% % Parameter vector (theta) note: unknown, estimated
% theta       = 0;
% thetaHat    = 0;
% thetaHatDot = 0;
% 
% % Desired trajectories
% % Collar Constants
% xDUpperBar = 1; % (m) Maximum aplitude of desired trajectory
% xDLowerBar = xDUpperBar; % (m) Minimum amplitude of desired trajectory
% fXD        = 1; % (Hz) Desired collar trajectory frequency
% % Collar desired trajectory
% xD       = xDUpperBar * sin(2 * pi * fXD * t_s); % (m) Desired position trajectory of collar
% xDDot    = 2 * pi * fXD * xDUpperBar * cos(2 * pi * fXD * t_s); % (m/s) Desired velocity trajectory of collar
% xDDotDot = - (2 * pi * fXD)^2 * xDUpperBar * sin(2 * pi * fXD * t_s); % (m/s^2) Desired velocity trajectory of collar
% % Pendulum desired constants
% phiDUpperBar = 0.79; % (rad) Maximum aplitude of desired trajectory (45deg nominal)
% phiDLowerBar = xDUpperBar(1); % (rad) Minimum amplitude of desired trajectory
% fPhiD        = 1; % (Hz) Desired pendulum trajectory frequency
% % Pendulum desired trajectory
% phiD       = phiDUpperBar * sin(2 * pi * fPhiD * t_s); % (m) Desired position trajectory of pendulum
% phiDDot    = 2 * pi * fPhiD * phiDUpperBar * cos(2 * pi * fPhiD_Hz * t_s); % (rad/s) Desired velocity trajectory of pendulum
% phiDDotDot = - (2 * pi * fPhiD)^2 * phiDUpperBar * sin(2 * pi * fPhiD * t_s); % (rad/s^2) Desired velocity trajectory of pendulum

%% Dynamics of collar and pendulum
xDotDot   = (1 / (mX + mPhi * sin(phi)^2)) * (-cX * xDot + mPhi * g * cos(phi) * sin(phi) + mPhi * l * phiDot)^2 + uX;
uXDot     = - aX * uX + muX;
phiDotDot = (1 / (mPhi * l^2)) * (-cPhi * phiDot - mPhi * l * g * sind(phi) - mPhi * l * cos(phi) * xDotDot + uPhi);
uPhiDot   = - aPhi * uPhi + muPhi;

% Define errors
% Collar
eX       = xD - x;
eXDot    = xDDot - xDot;
eXDotDot = xDDotDot - xDotDot;
% Pendulum
ePhi       = phi - x;
ePhiDot    = phiDDot - phiDot;
ePhiDotDot = phiDDotDot - phiDotDot;

% Filter tracking error
% collar
alphaX = 1; % Filter error coefficent
rX     = eXDot - alphaX * eX;
rXDot  = eXDotDot + alphaX * eXDot;
% Pendelum
alphaPhi = 1; % Filter error coefficent
rPhi     = ephiDot - alphaPhi * ePhi;
rPhiDot  = ePhiDotDot + alphaPhi * ePhiDot;

% Estimation parameters (parameter errors)
thetaTilde    = theta - thetaHat;
thetaTildeDot = -thetaHatDot;

aTilde    = a - aHat;
aTildeDot = - aHatDot;

uXTilde   = uXD - uX;
uPhiTilde = uPhiD - uPhi;

%% Simulate dynamics

% Autonomus open loop dynamics of collar and pendulum [x, phi], uX = uPhi = 0

% Parameters
mX = 1; mPhi = 1;
cX = 1; cPhi = 1;
aX = 1; aPhi = 1;

% Inputs (autonomus set to 0)
uX   = 0; uPhi  = 0;
muX  = 0; muPhi = 0;

% Define dynamics dXPhi = [xDot, xDotDot, xDotDotDot, phiDot, phiDotDot, phiDotDotDot]
function dXPhi = autoOpenLoopDynamics(xPhi, phi, mX, mPhi, cX, cPhi, uX, uPhi)%, aX, aPhi)
    dXPhi(2)    = (1 / (mX + mPhi * sin(phi(1))^2)) * (-cX * x(2) + mPhi * g * cos(phi(1)) * sin(phi(1)) + mPhi * l * phi(2))^2 + uX;
%     uX(2)   = - aX * uX(1) + muX;
    dXPhi(2 + 3)  = (1 / (mPhi * l^2)) * (-cPhi * phi(2) - mPhi * l * g * sind(phi(1)) - mPhi * l * cos(phi(1)) * x(3) + uPhi);
%     uPhi(2) = - aPhi * uPhi(1) + muPhi;
end


% % Needs to be in a form like this (from exam1)...
% dynamics = @(t,x) [-a * x(1) + x(2); ...
%                    -b * x(2) + x(3); ...
%                    -c * x(3)];
% 
% % Simulate dynamics
% [time, state] = ode45(dynamics, tSpan, initCond);

%% Simulate closed loop dynamics
% Follow 


