function [tL, axArray] = layoutExam1Problem1(states, parent)
% layoutExam1Problem1: Generates a layout of plots for exam 1 problem 1
% ======================================================================================================================
%                                                       INPUTS
% ======================================================================================================================
% ds:                                       A DataSource object
% parent:            (optional)             Handle to the parent object such as figure or uifigure onto which the plots
%                                           will be created
% ======================================================================================================================
%                                                       OUTPUTS
% ======================================================================================================================
% tL:                                       Handle to the tiled layout object
% axArray:                                  Array of axes that were generated
% ======================================================================================================================
%                                                    EXAMPLE USAGE
% ======================================================================================================================
% Plot to a new figure:
% layoutExam1Problem1(states)

arguments
    states
    parent = figure('Name', 'Problem 1');
end

tL = tiledlayout(parent, 3, 1, 'Padding', 'compact', 'TileSpacing', 'none');

title(tL, "Problem 1");
nexttile(1)


nexttile(1)


nexttile(1)


axArray = findall(tL.Children, 'Type', 'axes');
end
